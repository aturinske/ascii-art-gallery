# ASCII Art Gallery

Shows ASCII art in your terminal

## Usage

1. Clone the repo to a local directory
2. Create an alias or export it to run the script with the desired gallery
```bash
alias dinoArt=/path/to/itsasciitime.sh
```
or if one has added another gallery
```bash
alias newArt=/path/to/itsasciitime.sh galleries/new new_
```
3. Integrate the alias/function to run every time you do a common command
  - [githooks](https://git-scm.com/docs/githooks) (e.g. [creating a commit](https://git-scm.com/docs/githooks#_post_commit), [pushing code up to a repository](https://git-scm.com/docs/githooks#post-update), etcetera)
```bash
➜  ascii-art-gallery git:(test) vim .git/hooks/post-commit
➜  ascii-art-gallery git:(test) touch test.txt
➜  ascii-art-gallery git:(test) ✗ ga .
➜  ascii-art-gallery git:(test) ✗ gc
                         .       .
                        / `.   .' \
                .---.  <    > <    >  .---.
                |    \  \ - ~ ~ - /  /    |
                 ~-..-~             ~-..-~
             \~~~\.'                    `./~~~/
   .-~~^-.    \__/                        \__/
 .'  O    \     /               /       \  \
(_____,    `._.'               |         }  \/~~~/
 `----.          /       }     |        /    \__/
       `-.      |       /      |       /      `. ,~~|
           ~-.__|      /_ - ~ ^|      /- _      `..-'   f: f:
                |     /        |     /     ~-.     `-. _||_||_
                |_____|        |_____|         ~ - . _ _ _ _ _>
[test 4c27156] test commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test.txt
On branch test
nothing to commit, working tree clean
➜  ascii-art-gallery git:(test)
```

## Adding new galleries
1. Create a new folder in the [galleries folder](https://gitlab.com/aturinske/ascii-art-gallery/-/tree/default/galleries) (e.g. `bird`, `plant`, etcetera)
2. Add new art to the new folder, following the convention shown in [the dino gallery](https://gitlab.com/aturinske/ascii-art-gallery/-/tree/default/galleries/dino) (e.g. `plant_1`, `plant_2`, etcetera)
3. Call [`itsasciitime`](https://gitlab.com/aturinske/ascii-art-gallery/-/blob/default/itsasciitime.sh) with the appropriate path and name (e.g. `/path/to/itsasciitime.sh galleries/bird bird_`, etcetera)
