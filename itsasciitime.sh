#!/bin/bash

#===   FUNCTION   ===============================
#          NAME: itsasciitime
#   DESCRIPTION: Displays an ASCII masterpiece from a chosen repo containing ASCII art
#   PARAMETER 1: path to ASCII art gallery (defaults to `galleries/dino`)
#   PARAMETER 2: naming convention for ASCII art (defaults to `dino_`)
#================================================
function itsasciitime {
    # Set defaults
    defaultPath='galleries/dino'
    defaultNamingConvention='dino_'

    # Determine path and naming convention
    path=$([ "$1" ] && echo "$1" || echo $defaultPath)
    namingConvention=$([ "$2" ] && echo "$2" || echo $defaultNamingConvention)

    # Find athe number of art pieces in the gallery to randomly pick from
    maxNumString=$(ls -l ./${path} | grep -v ^d | grep -v ^t | wc -l)
    maxNum=${maxNumString//[!0-9]/}

    # Choose a piece
    chosen=$(shuf -i 1-${maxNum} -n 1)
    chosenFile=$(pwd)"/${path}/${namingConvention}"${chosen}".txt"

    # Display the chosen art piece
    cat $chosenFile
}

itsasciitime $1 $2
